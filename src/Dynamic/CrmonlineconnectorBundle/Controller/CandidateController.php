<?php
namespace Dynamic\CrmonlineconnectorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Dynamic\CrmonlineconnectorBundle\Entity\Candidate;
use Dynamic\CrmonlineconnectorBundle\Form\Type\CandidateType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class CandidateController extends Controller{
    
    /**
     * @Route("/register", name="registrationpage")
     */  
    public function registrationAction(Request $request){
        $candidate = new Candidate();
        $form = $this->createForm(CandidateType::class, $candidate);  
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {           
            $candidate = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($candidate);
            $em->flush();
            $url = $this->generateUrl('successpage');
            $response = new RedirectResponse($url);
            return $response;
        }
        
        return $this->render('DynamicCrmonlineconnectorBundle:Registration:register.html.twig', array(
                    'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/list", name="candidatelist")
     */  
    public function candidateListAction(){
       $em = $this->getDoctrine()->getManager();
       $candidates = $em->getRepository('DynamicCrmonlineconnectorBundle:Candidate')->findAll();   
       return $this->render('DynamicCrmonlineconnectorBundle:Registration:candidatelist.html.twig', array(
                        'candidates' => $candidates,
            ));
        
    }
    /**
     * @Route("/success", name="successpage")
     */
    public function successAction(){
      return $this->render('DynamicCrmonlineconnectorBundle:Registration:success.html.twig');     
    }  
    
    /**
     * @Route("/edit/{id}", requirements={"id" = "\d+"}, name="candidateeditpage")
     */
    public function editCandidateAction(Request $request, $id){ 
      $em = $this->getDoctrine()->getManager();
      $candidate = $em->getRepository('DynamicCrmonlineconnectorBundle:Candidate')->findOneBy(array('id' => $id));
      $form = $this->createForm(CandidateType::class, $candidate);  
      $form->handleRequest($request);
      
      if ($form->isSubmitted() && $form->isValid()) {           
            $candidate = $form->getData();
            $em->persist($candidate);
            $em->flush();
            $url = $this->generateUrl('candidatelist');
            $response = new RedirectResponse($url);
            return $response;
        }
        
        return $this->render('DynamicCrmonlineconnectorBundle:Registration:candidateeditpage.html.twig', array(
                    'form' => $form->createView(),
                    'id' => $id,
        ));  
    } 
    
    /**
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="candidatedeletepage")
     */
    public function deleteCandidateAction(Request $request, $id){ 
      $em = $this->getDoctrine()->getManager();
      $candidate = $em->getRepository('DynamicCrmonlineconnectorBundle:Candidate')->findOneBy(array('id' => $id));
      $em->remove($candidate);
      $em->flush();
      return $this->render('DynamicCrmonlineconnectorBundle:Registration:candidatedeletepage.html.twig');  
    } 
    
    /**
     * @Route("/connectcrm", name="connectcrm")
     */  
    public function connectcrmAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        //There will be candidate id sent for this method, so retrieve it
        $newsletterid = 1;
        $crmclient = $this->container->get('crmclient_utility');
        $crmclient->constructDynamics();
        $crmclient->doOCPAuthentication();
        //Pass the action for the crm record
        $soapheader = $crmclient->getCRMSoapHeader('Create');
        $newsletter = $em->getRepository('DynamicCrmonlineconnectorBundle:Candidate', $newsletterid);
        $soapbody = $crmclient->getSoapCreateNewsletterBody($newsletter);
        $serverResponse = $crmclient->sendQuery($soapheader, $soapbody);

//        $this->logger->addInfo($serverResponse);

        //Retrieve response and update the crm record
        $iscrmrecord = $crmclient->isCrmRecordCreated($serverResponse);

        if ($iscrmrecord) {
//            $newsletter->setIscrmrecord(TRUE);
//            $newsletter->setCrmcreatedon(new \DateTime("now", new \DateTimeZone(date_default_timezone_get())));
//            $this->em->flush();
//            return TRUE;
            return new Response('Success');
        } else {
            return FALSE;
        }
        
    }
} 
